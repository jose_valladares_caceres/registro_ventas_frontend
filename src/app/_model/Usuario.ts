export class Usuario{
  // @ts-ignore
  idUsuario: number;
  // @ts-ignore
  nombre: string | undefined;
  apellido_paterno: string | undefined;
  apellido_materno: string | undefined;
  usuario: string | undefined;
  contrasenia: string | undefined;
  email: string | undefined;
}
