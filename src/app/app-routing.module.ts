import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsuarioComponent} from './pages/usuario/usuario.component';
import {UsuarioEdicionComponent} from './pages/usuario/usuario-edicion/usuario-edicion.component';
import {ProductoComponent} from './pages/producto/producto.component';
import {ProductoEdicionComponent} from './pages/producto/producto-edicion/producto-edicion.component';
import {OperacionComponent} from './pages/operacion/operacion.component';
import {OperacionEdicionComponent} from './pages/operacion/operacion-edicion/operacion-edicion.component';
import {LoginComponent} from './pages/login/login.component';

const routes: Routes = [
  {path:'login', component: LoginComponent},
  {
    path:'usuario', component: UsuarioComponent,
    children:[
      { path:'nuevo', component: UsuarioEdicionComponent },
      { path:'edicion/:id', component: UsuarioEdicionComponent}
    ]
  },
  {
    path:'producto', component: ProductoComponent,
    children:[
      { path:'nuevo', component: ProductoEdicionComponent },
      { path:'edicion/:id', component: ProductoEdicionComponent}
    ]
  },
  {
    path:'operacion', component: OperacionComponent,
    children:[
      { path:'nuevo', component: OperacionEdicionComponent },
      { path:'edicion/:id', component: OperacionEdicionComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
