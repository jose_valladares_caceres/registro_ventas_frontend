import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {UsuarioService} from '../../../_service/usuario.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Usuario} from '../../../_model/Usuario';
import {BooleanInput} from '@angular/cdk/coercion';

@Component({
  selector: 'app-usuario-edicion',
  templateUrl: './usuario-edicion.component.html',
  styleUrls: ['./usuario-edicion.component.css']
})
export class UsuarioEdicionComponent implements OnInit {
  // @ts-ignore
  form: FormGroup;
  id: number = 0;
  edicion: Boolean|undefined;
  userEditable!: BooleanInput;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id':new FormControl(0),
      'nombre': new FormControl(''),
      'apellido_paterno': new FormControl(''),
      'apellido_materno': new FormControl(''),
      'usuario': new FormControl(''),
      'email': new FormControl('')
    });
    this.route.params.subscribe((data:Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.userEditable = (this.edicion == true);
      this.initForm();
    });
  }

  operar() {
    let usuario = new Usuario();
    usuario.idUsuario = this.form.value['id'];
    usuario.nombre = this.form.value['nombre'];
    usuario.apellido_paterno = this.form.value['apellido_paterno'];
    usuario.apellido_paterno = this.form.value['apellido_materno'];
    usuario.usuario = this.form.value['usuario'];
    usuario.email = this.form.value['email'];

    if(this.edicion){
      //Modificar
      this.userEditable = false;
      this.usuarioService.modificar(usuario).subscribe(
        // Metodo no devuelve nada por eso se usa ()=>
        () => {
          this.usuarioService.listar().subscribe(data => {
            this.usuarioService.usuarioCambio.next(data);
            this.usuarioService.mensajeCambio.next("Se modifico");
          });
        }
      );
    }else{
      //Insertar
      this.userEditable = true;
      this.usuarioService.registrar(usuario).subscribe(
        () => {
          this.usuarioService.listar().subscribe(data => {
            this.usuarioService.usuarioCambio.next(data);
            this.usuarioService.mensajeCambio.next("Se registro");
          });
        }
      );
    }
    this.router.navigate(['usuario']);
  }

  private initForm() {
    //Editar o modificar, por lo tanto carga la data inicial
    if(this.edicion){
      this.usuarioService.listarPorId(this.id).subscribe(data =>{
        console.log(data);
        this.form = new FormGroup({
          'id':new FormControl(data.idUsuario),
          'nombre': new FormControl(data.nombre),
          'apellido_paterno': new FormControl(data.apellido_paterno),
          'apellido_materno': new FormControl(data.apellido_materno),
          'usuario': new FormControl(data.usuario),
          'email': new FormControl(data.email)
        });
      });
    }
  }
}
