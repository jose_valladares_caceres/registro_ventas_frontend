import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {UsuarioService} from '../../_service/usuario.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Usuario} from '../../_model/Usuario';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  dataSource: MatTableDataSource<Usuario> = new MatTableDataSource<Usuario>();
  displayedColumns: any = ['idUsuario', 'usuario', 'acciones'];

  // @ts-ignore
  @ViewChild(MatSort, {static: true}) sort: MatSort
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private usuarioService: UsuarioService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {

    this.usuarioService.listar().subscribe( data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });


    this.usuarioService.usuarioCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.usuarioService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, "AVISO",{
        duration:2000
      });
    });


  }

  eliminar(idUsuario: number) {
    this.usuarioService.eliminar(idUsuario).subscribe(
      ()=>{
        this.usuarioService.listar().subscribe(data => {
          this.usuarioService.usuarioCambio.next(data);
          this.usuarioService.mensajeCambio.next('Se elimino el registro');
        });
      }
    );
  }

  filtrar(myEvent: any) {
    this.dataSource.filter = myEvent.target.value.trim().toLowerCase();
  }
}
