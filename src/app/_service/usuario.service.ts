import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Usuario} from '../_model/Usuario';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuarioCambio: Subject<Usuario[]> = new Subject<Usuario[]>();
  mensajeCambio: Subject<string> = new Subject<string>();
  url = `${environment.HOST}/usuario`;

  constructor(
    private http: HttpClient
  ) { }

  listar(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(this.url);
  }

  listarPorId(id:number): Observable<Usuario>{
    return this.http.get<Usuario>(`${this.url}/${id}`);
  }

  registrar(usuario: Usuario){
    return this.http.post(this.url, usuario);
  }

  modificar(usuario: Usuario){
    return this.http.put(this.url, usuario);
  }

  eliminar(id: number){
    return this.http.delete(`{this.url}/${id}`);
  }

}
